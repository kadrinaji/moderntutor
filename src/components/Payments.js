import React from 'react'
import { Route, Switch } from 'react-router-dom';
import './Listings.css';
import PaymentEditor from './PaymentEditor';
import PaymentsList from './PaymentsList';

export default function Payments() {
    return (
        <Switch>
            <Route path="/payments/:id" component={PaymentEditor}></Route>
            <Route path="/payments/" component={PaymentsList}></Route>
        </Switch>
    )
}
