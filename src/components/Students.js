import React from 'react'
import { StudentContext } from '../contexts/StudentContext';
import './Listings.css';
import { FaEdit, FaCheckCircle, FaTimesCircle, FaTrash, FaPlus } from 'react-icons/fa';

export default function Students() {

    return (
        <StudentContext.Consumer>{(context) => {
            return (
                <div>
                    <h1 className="title">Students</h1>
                    <div className="students-container">
                        <div className="students-list">
                            {context.students.map(student => {
                                if (student.editing) {
                                    return (
                                        <div className="student-item card-1" key={student.name}>
                                            <div><p><b>Student:</b></p> <input type="text" name="student" value={context.editStudent.name} onChange={context.handleChange} autoComplete="off"></input></div>
                                            <div><p><b>Univesrity:</b></p> <input type="text" name="university" value={context.editStudent.university} onChange={context.handleChange} autoComplete="off"></input></div>
                                            <div>
                                                <button className="saveBtn" onClick={() => { context.confirmEdit(student.name) }}><FaCheckCircle size={25} color="#EEFBFB"></FaCheckCircle></button>
                                                <button className="cancelBtn" onClick={context.cancelEdit}><FaTimesCircle size={25} color="#203647"></FaTimesCircle></button>
                                            </div>
                                        </div>
                                    )
                                } else {
                                    return (<div className="student-item card-1" key={student.name}>
                                        <p className="student-name"> {student.name}</p>
                                        <p> <b>University:</b>  {student.university}</p>
                                        {context.editing ? <div></div> :
                                            <div className="buttons">
                                                <button className="renameBtn" onClick={() => { context.edit(student.name) }}><FaEdit size={25} color="white"></FaEdit></button>
                                                <button className="deleteBtn" onClick={() => { context.remove(student.name) }}><FaTrash size={25} color="white"></FaTrash></button>
                                            </div>
                                        }
                                    </div>);
                                }
                            })}
                        </div>
                        <button className="addBtn card-3" onClick={context.addStudent}><FaPlus className="addBtnIcon" size={25} color="#EEFBFB"></FaPlus></button>
                    </div>
                </div>
            )
        }}</StudentContext.Consumer>
    )
}
