import React, { useState } from 'react';
import { PaymentContext } from '../contexts/PaymentContext';
import './Summary.css';

export default function Summary() {

    const [monthlyIncome, setMonthlyIncome] = useState(0);

    return (
        <PaymentContext.Consumer>{context =>
            <div className="summary">
                <h1>Summary</h1>

                <div className="data">
                    <h1 className="dataval">{context.getTotalUnpaid().toLocaleString()} LBP</h1>
                    <h3>Total Unpaid Amount</h3>
                </div>

                <div className="data">
                    <h1 className="dataval">{context.getTotalIncome().toLocaleString()} LBP</h1>
                    <h3>Total Income</h3>
                </div>

                <div className="data">
                    <h1 className="dataval">{context.getTotalHours().toLocaleString()}</h1>
                    <h3>Total Hours</h3>
                </div>

            </div>
        }</PaymentContext.Consumer>
    )


}
