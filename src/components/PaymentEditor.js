import React from 'react'
import { useParams, useHistory } from 'react-router-dom';
import { FaArrowLeft } from 'react-icons/fa';
import './Listings.css';
import { PaymentContext } from '../contexts/PaymentContext';
import TimeRangePicker from '@wojtekmaj/react-timerange-picker';
import DatePicker from 'react-date-picker';
import DataFormatter from '../formatters/DataFormatter';
import {FaTrash, FaPlus } from 'react-icons/fa';

export default function PaymentEditor() {

    const history = useHistory();

    let { id } = useParams();

    const backToPayments = () => {
        history.push('/payments');
    }

    return (
        <PaymentContext.Consumer>{(context) => {
            const payment = context.getPayment(id);

            if(payment === undefined)
                return null;

            return (
                <div>
                    <button className="backBtn card-3" onClick={backToPayments}><FaArrowLeft size={30} color="#EEFBFB"></FaArrowLeft></button>
                    <h1 className="title" style={{ paddingTop: '5px' }}>Payment #{id}</h1>
                    <div className="payments-container">
                        <div className="session-list">
                            <div className="session-overview">
                                <h3>Student: {payment.student}</h3>
                                <div>
                                    <h3 className="rate-text">LBP/$: </h3>
                                    <input name="rate" type="number" className="rate-input" value={payment.rate} onChange={(e) => { context.handleRateChange(e, payment.id) }}></input>
                                </div>
                            </div>
                            <div className="session-overview">
                                <h3>Total Hours: {context.calculateTotalHours(payment.id)} </h3>
                                <h3>Total Earnings: {context.calculateTotalEarnings(payment.id).toLocaleString()}$ / {context.calculateTotalEarnings(payment.id, false).toLocaleString()} LBP </h3>
                            </div>
                            {payment.sessions.map((session) => {
                                return (
                                    <div className="session-data" key={session.id}>
                                        <div className="session-item card-3">
                                            <div>
                                                <span>Date: </span>
                                                <DatePicker value={session.date} onChange={(value) => { context.onDateChange(payment.id, session.id, value) }}></DatePicker>
                                            </div>
                                            <div>
                                                <span>Time: </span>
                                                <TimeRangePicker value={session.time} onChange={(value) => { context.onTimeChange(payment.id, session.id, value) }}></TimeRangePicker>
                                            </div>
                                            <div>
                                                <button className="deleteBtn" name="deleteSession" onClick={() => {context.removeSession(payment.id, session.id)}}><FaTrash size={25} color="#203647"></FaTrash></button>
                                            </div>
                                        </div>
                                        <div className="session-item card-2">
                                            <p className="session-info">Session Hours: {DataFormatter.calculateHours(session.time)} </p>
                                            <p className="session-info">Session Earnings: {DataFormatter.calculateEarnings(session.time, context.dph, payment.rate).toLocaleString()}$ / {DataFormatter.calculateEarnings(session.time, context.dph, payment.rate, false).toLocaleString()} LBP</p>
                                        </div>
                                    </div>
                                )
                            })
                            }
                        </div>

                        <button className="addBtn card-3" onClick={() => {context.addSession(payment.id)}}><FaPlus className="addBtnIcon" size={25} color="#EEFBFB"></FaPlus></button>

                    </div>
                </div>
            )
        }

        }

        </PaymentContext.Consumer>
    )
}
