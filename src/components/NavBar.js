import React from 'react';
import {Link} from 'react-router-dom';

export default function NavBar() {

    return (
        <div id="navbar" className="card-4">
            <h1><Link to="/" className="link" >The Modern Tutor</Link></h1>
            <div id="navlinks">
                    <div className="navlink"><Link to="/students" className="link">Students</Link></div>
                    <div className="navlink"><Link to="/payments"  className="link">Payments</Link></div>
            </div>
        </div>
    );
}