import React, { useState } from 'react'
import { PaymentContext } from '../contexts/PaymentContext';
import { StudentContext } from '../contexts/StudentContext';
import { FaTrash, FaPlus } from 'react-icons/fa';
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import SelectSearch from 'react-select-search';
import './SelectSearch.css';

export default function PaymentsList() {

    const [popupOpen, setPopupOpen] = useState(false);

    const [selectedStudent, setSelectedStudent] = useState('');

    const [paymentFilter, setPaymentFilter] = useState('all');

    const filterOptions = [
        { name: 'All', value: 'all' },
        { name: 'Paid', value: 'paid' },
        { name: 'Unpaid', value: 'unpaid' }
    ]

    const canView = paid => (paymentFilter === 'all')||(paymentFilter === 'paid' && paid)||(paymentFilter === 'unpaid' && !paid);

    const closeModal = () => setPopupOpen(false);

    const cancelNewPayment = () => {
        closeModal();
    }

    const confirmNewPayment = (paymentCtx) => {
        if(selectedStudent !== ''){
            paymentCtx.addPayment(selectedStudent);
        }
        closeModal();
    }

    return (
        <StudentContext.Consumer>{(studentCtx) =>
            <PaymentContext.Consumer>{(context) => {
                return (
                    <div>
                        <h1 className="title">Payments</h1>
                        <div className="payments-container">
                            <div className="payment-list">
                               <div className="payment-item">
                                   <SelectSearch options={filterOptions} value={paymentFilter} onChange={setPaymentFilter} />
                               </div>
                                {context.payments.map((payment) => {
                                    if(canView(payment.paid)){
                                        return (
                                            <div className="payment-item card-1" key={payment.id} onClick={() => { context.handlePaymentSelection(payment.id) }}>
                                                <p className="payment-nb">#{payment.id}</p>
                                                <p>{payment.student}</p>
                                                <p>LBP/$: {payment.rate}</p>
                                                <p>Hours: {context.calculateTotalHours(payment.id)}</p>
                                                <p>Earnings: {context.calculateTotalEarnings(payment.id, false).toLocaleString()} LBP</p>
                                                <div>
                                                    <p style={{ display: 'inline-block' }}>Paid: </p>
                                                    <input className="payment-input" name="paid" type="checkbox" paymentid={payment.id} checked={payment.paid} onChange={context.handleStatusChange} />
                                                </div>
                                                <div>
                                                    <button className="deletePaymentBtn" name="deletePayment" onClick={(e) => { context.removePayment(payment.id); e.stopPropagation() }}><FaTrash size={25} color="#EEFBFB"></FaTrash></button>
                                                </div>
                                            </div>
                                        )
                                    }else{
                                        return null;
                                    }
                                })}
                            </div>
                            <button className="addBtn card-3" onClick={() => { setPopupOpen(true) }}><FaPlus className="addBtnIcon" size={25} color="#EEFBFB"></FaPlus></button>
                            <Popup modal closeOnDocumentClick={false} open={popupOpen} onClose={closeModal}>
                                <div className="students-popup">
                                    <h1 className="title">New Payment</h1>
                                    <h3>Student:</h3>
                                    {/* add "search" attribute to selectsearch to search options, you also need filterOptions function */}
                                    <SelectSearch options={studentCtx.listStudentsOptions()} value={selectedStudent} onChange={setSelectedStudent} emptyMessage="Student not found" placeholder="Select student" />
                                    <div>
                                        <button className="confirmBtn popup-button" onClick={() => { confirmNewPayment(context) }}>Confirm</button>
                                        <button className="cancelBtn popup-button" onClick={cancelNewPayment}>Cancel</button>
                                    </div>
                                </div>
                            </Popup>
                        </div>
                    </div>
                )
            }
            }</PaymentContext.Consumer>
        }</StudentContext.Consumer>
    )

}
