import React, {createContext, Component} from 'react';

export const StudentContext = createContext();

function createStudent(name, uni, editing = false){
    return {
        name: name,
        university: uni,
        editing: editing
    }
}

// createStudent('Dummy A', 'UoT'), createStudent('Dummy B', 'MIT')7

export default class StudentContextProvider extends Component {

    state = { students: [], 
    editing: false, editStudent: {name: '', university: ''} };

    methods = {

        edit : (name) => {

            if(this.state.editing)
                return;
    
            let uni;
    
            const students = this.state.students.map( (student) => {
                if(student.name === name){
                    uni = student.university;
                    return createStudent(student.name, student.university, true);
                }else{
                    return student;
                }
            });
    
            this.setState({students: students, editing: true, editStudent: {name: name, university: uni}});
        },

        remove : (name) => {

            if(this.state.editing)
                return;

            const students = this.state.students.filter( (student) => student.name !== name);

            this.setState({...this.state, students: students})
        },

        addStudent : () => {
            if(this.state.students.filter( (student) => student.name === 'new student').length === 0){
                const students = this.state.students.concat(createStudent('new student', 'uni', true));
                this.setState({...this.state, students: students});
            }
        },
    
        handleChange : e => {
            if(e.target.getAttribute('name') === 'student'){
                this.setState({...this.state, editStudent: {name: e.target.value, university: this.state.editStudent.university}});
            }else{
                this.setState({...this.state, editStudent: {name: this.state.editStudent.name, university: e.target.value}});
            }
        },
    
        cancelEdit : () => {
            const students = this.state.students.map( (student) => {
                return createStudent(student.name, student.university, false);
            })
            this.setState({students: students, editing: false, editStudent: {name: '', university: ''}});
        },

        confirmEdit: (name) => {
            
            const students = this.state.students.map( (student) => {
                if(student.name === name){
                    return createStudent(this.state.editStudent.name, this.state.editStudent.university, false);
                }else{
                    return student;
                }
            });

            this.setState({students: students, editing: false,  editStudent: {name: '', university: ''}});
        },

        listStudentsOptions: () => this.state.students.map( (student) => {
            return {
                name: student.name,
                value: student.name,
            }
        })
    }

    componentDidMount(){
        const students = localStorage.getItem('tmt.students');

        if(students == null){
            localStorage.setItem('tmt.students', JSON.stringify([]));
        }else{
            this.setState({...this.state, students: JSON.parse(students)});
        }
    }

    componentDidUpdate(prevProps, prevState){

        if(this.state.students !== prevState.students && 
            this.state.students.filter( (student) => student.name === 'new student').length === 0){

            const students = this.state.students.map( (student) => {
                return createStudent(student.name, student.university);
            });

            localStorage.setItem('tmt.students', JSON.stringify(students));
        }
    }

    render() {
        return (
            <StudentContext.Provider value={{...this.state, ...this.methods}}>
                {this.props.children}
            </StudentContext.Provider>
        )
    }

}
