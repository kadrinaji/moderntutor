import React, { createContext, Component } from 'react'
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import DataFormatter from '../formatters/DataFormatter';

export const PaymentContext = createContext();

class PaymentContextProvider extends Component {

    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    };

    state = {
        payments: []
    }

    methods = {

        handleStatusChange: e => {
            const id = e.target.getAttribute('paymentid') - 0;
            const payments = this.state.payments.map((payment) => {
                if (payment.id === id) {
                    return { ...payment, paid: !payment.paid }
                } else {
                    return payment;
                }
            })

            this.setState({ ...this.state, payments: payments });
        },

        handleRateChange: (e, pid) => {
            const payments = this.state.payments.map((payment) => {
                if (payment.id === pid) {
                    return { ...payment, rate: e.target.value - 0 };
                } else {
                    return payment;
                }
            });

            this.setState({ ...this.state, payments: payments });
        },

        handlePaymentSelection: (id) => {
            this.props.history.push(`/payments/${id}`);
        },

        removeSession: (pid, id) => {
            let upayment = { ...this.state.payments.find((payment) => payment.id === pid) };

            upayment.sessions = upayment.sessions.filter((session) => session.id !== id);

            const payments = this.state.payments.map((payment) => {
                if (payment.id === pid) {
                    return upayment;
                } else {
                    return payment;
                }
            })

            this.setState({ ...this.state, payments: payments });

        },

        removePayment: pid => {

            const payments = this.state.payments.filter((payment) => payment.id !== pid);

            this.setState({ ...this.state, payments: payments });

        },

        addSession: pid => {

            let upayment = { ...this.state.payments.find((payment) => payment.id === pid) };

            const sid = upayment.sid + 1;

            upayment.sessions = upayment.sessions.concat({
                id: sid,
                date: new Date(),
                time: ['12:00', '12:00']
            })

            upayment.sid = sid;

            const payments = this.state.payments.map((payment) => {
                if (payment.id === pid) {
                    return upayment;
                } else {
                    return payment;
                }
            })

            this.setState({ ...this.state, payments: payments });
        },

        addPayment: (name) => {

            const pid = this.state.pid + 1;
            const payments = this.state.payments.concat({
                id: pid,
                student: name,
                rate: 0,
                paid: false,
                sid: 0,
                sessions: []
            })

            this.setState({ ...this.state, payments: payments, pid: pid });
        },

        getPayment: id => {
            return this.state.payments.find((payment) => payment.id === (id - 0));
        },

        onDateChange: (pid, id, value) => {

            let upayment = { ...this.state.payments.find((payment) => payment.id === pid) };
            upayment.sessions = upayment.sessions.map((session) => {
                if (session.id === id) {
                    return { ...session, date: value };
                } else {
                    return session;
                }
            });

            const payments = this.state.payments.map((payment) => {
                if (payment.id === pid) {
                    return upayment;
                } else {
                    return payment;
                }
            })

            this.setState({ ...this.state, payments: payments });

        },

        onTimeChange: (pid, id, value) => {

            let upayment = { ...this.state.payments.find((payment) => payment.id === pid) };
            upayment.sessions = upayment.sessions.map((session) => {
                if (session.id === id) {
                    return { ...session, time: value };
                } else {
                    return session;
                }
            });

            const payments = this.state.payments.map((payment) => {
                if (payment.id === pid) {
                    return upayment;
                } else {
                    return payment;
                }
            })

            this.setState({ ...this.state, payments: payments });
        },

        calculateTotalHours: pid => {

            let upayment = { ...this.state.payments.find((payment) => payment.id === pid) };

            let hours = upayment.sessions.map((session) => DataFormatter.calculateHours(session.time));

            hours = hours.reduce((a, b) => a + b, 0);

            return hours;

        },

        calculateTotalEarnings: (pid, inDollar = true) => {

            let upayment = { ...this.state.payments.find((payment) => payment.id === pid) };

            let hours = upayment.sessions.map((session) => DataFormatter.calculateHours(session.time));

            hours = hours.reduce((a, b) => a + b, 0);

            return hours * this.state.dph * (inDollar ? 1 : upayment.rate);

        },

        getTotalUnpaid: () => {
            let unpaid = this.state.payments.filter((payment) => !payment.paid);

            unpaid = unpaid.map((payment) => this.methods.calculateTotalEarnings(payment.id, false));

            unpaid = unpaid.reduce((a, b) => a + b, 0);

            return unpaid;
        },

        getTotalIncome: () => {
            let paid = this.state.payments.filter((payment) => payment.paid);

            paid = paid.map((payment) => this.methods.calculateTotalEarnings(payment.id, false));

            paid = paid.reduce((a, b) => a + b, 0);

            return paid;
        },

        getTotalHours: () => {
            let hours = this.state.payments.map((payment) => this.methods.calculateTotalHours(payment.id));
            return hours.reduce((a, b) => a + b, 0);
        }
    }

    componentDidMount() {

        const paymentCtx = localStorage.getItem('tmt.payments');

        if (paymentCtx == null) {

            const defaultState = {
                payments: [],
                pid: 0,
                dph: 20
            };

            localStorage.setItem('tmt.payments', JSON.stringify(defaultState));

            this.setState(defaultState);

        } else {
            this.setState(JSON.parse(paymentCtx, (key, value) => {
                if (key === 'date') {
                    return new Date(value);
                } else {
                    return value;
                }
            }))
        }
    }

    componentDidUpdate(prevProps, prevState) {

        if (this.state !== prevState) {
            localStorage.setItem('tmt.payments', JSON.stringify(this.state))
        }


    }

    render() {
        return (
            <PaymentContext.Provider value={{ ...this.state, ...this.methods }}>
                {this.props.children}
            </PaymentContext.Provider>
        )
    }


}

export default withRouter(PaymentContextProvider);

// payment context state example

/*         payments: [
            {
                id: 1,
                student: 'Dummy A',
                rate: 3000,
                paid: true,
                sid: 0,
                sessions: []
            },
            {
                id: 2,
                student: 'Dummy B',
                rate: 3850,
                paid: false,
                sid: 1,
                sessions: [
                    {
                        id: 1,
                        date: new Date(),
                        time: ['10:00', '11:00']
                    }
                ]
            }
        ],
        pid: 2,
        dph: 20 // dollars per hour */