import NavBar from './components/NavBar';
import './Main.css';
import { Route, Switch } from 'react-router-dom';
import Summary from './components/Summary';
import Students from './components/Students';
import Payments from './components/Payments';
import StudentContextProvider from './contexts/StudentContext';
import PaymentContextProvider from './contexts/PaymentContext';

function App() {
  return (
    <div className="App">
      <header>
        <NavBar />
      </header>

      <main>
        <StudentContextProvider>
          <PaymentContextProvider>
            <Switch>
              <Route path="/students" component={Students}></Route>
              <Route path="/payments" component={Payments}></Route>
              <Route path="/" component={Summary}></Route>
            </Switch>
          </PaymentContextProvider>
        </StudentContextProvider>
      </main>
    </div>
  );
}

export default App;
