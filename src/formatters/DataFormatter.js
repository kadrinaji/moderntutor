// this class is used to format data and do some calculations

export default class DataFormatter {

    static calculateHours(time){

        let timeStart = new Date('01/01/2007 ' + time[0]);
        let timeEnd = new Date('01/01/2007 ' + time[1]);
        let diff = (timeEnd - timeStart) / 60 / 60 / 1000;
        let result = Math.round((diff + Number.EPSILON) * 100) / 100;
        return (result < 0 ? 24 + result : result);
    }

    static calculateEarnings (time, dph, rate, inDollar = true){
        return this.calculateHours(time) *  dph * (inDollar ? 1 : rate);
    }

}